import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('autoIndex', false);

mongoosePaginate.paginate.options = {
  collation: { locale: 'en', strength: 1 },
  sort: { label: 'asc' },
  leanWithId: true
};
