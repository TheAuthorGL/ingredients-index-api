import dotenv from 'dotenv';

dotenv.config();

export const defaultServerOptions = {
  port: process.env.PORT || 8080,
  databaseUrl: process.env.DATABASE_URL,
  defaultUser: process.env.USER,
  jwtSecret: process.env.JWT_SECRET || 'secret'
};

export function fillServerOptions(providedOptions = {}) {
  return { ...defaultServerOptions, ...providedOptions };
}
