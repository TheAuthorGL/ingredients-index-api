import { promisify } from 'util';
import { promises as fs } from 'fs';
import express from 'express';
import http from 'http';
import cors from 'cors';
import mongoose from 'mongoose';
import chalk from 'chalk';
import { renderToString } from '@vue/server-renderer';

import { log } from '@shared/log';
import { fillServerOptions } from './server-options';
import './mongoose-configuration';
import { modules } from '@modules/index';
import { createFrontendVueApp } from '@frontend/create-app';

// TODO: Separate content into other file
export function createApp(options = {}) {
  const app = express();

  app.enable('trust proxy');
  app.use(cors());

  app.use((req, res, next) => {
    if (process.env.NODE_ENV !== 'production' || req.secure) {
      next();
    } else {
      res.redirect(`https://${req.headers.host}${req.url}`);
    }
  });

  app.use(express.json());
  app.use('/resources/frontend', express.static('./dist/frontend'));
  app.use('/resources/admin', express.static('./dist/admin'));

  modules
    .filter((module) => module.setup)
    .forEach((module) => {
      module.setup(options);
    });

  modules
    .filter((module) => module.createRouter)
    .forEach((module) => {
      app.use('/api', module.createRouter(options));
    });

  app.get('/robots.txt', (req, res) => {
    res.type('text/plain');
    res.send(`
      User-agent: *
      Disallow: /admin/*
    `);
  });

  app.get([ '/admin', '/admin/*' ], async (req, res) => {
    const htmlContent = await fs.readFile('./admin/index.html', { encoding: 'UTF-8' });

    res.send(htmlContent);
  });

  // TODO: This should also serve the favicon, and should be separated.
  app.get('*', async (req, res) => {
    const { app: vueApp, router: vueAppRouter } = createFrontendVueApp();

    await vueAppRouter.push(req.url);
    await vueAppRouter.isReady();

    const vueAppContent = await renderToString(vueApp);
    // TODO: This should be loaded only once for optimization purposes.
    const htmlTemplate = await fs.readFile('./frontend/index.html', { encoding: 'UTF-8' });

    res.setHeader('Content-Type', 'text/html');
    res.send(htmlTemplate.replace('{{ appContent }}', vueAppContent));
  });

  app.use((error, req, res, next) => {
    if (error.name === 'ValidationError') {
      const errors = Object.fromEntries(Object.entries(error.errors).map(([ key, value ]) => [
        key,
        value.message
      ]));

      return res.status(400).json(errors);
    }

    console.error(error);
    res.status(500).send('An unknown error has occurred.');
    next();
  });

  return app;
}

export async function startServer(providedOptions = {}) {
  const options = fillServerOptions(providedOptions);

  log(chalk`{gray Starting server...}`);

  await mongoose.connect(options.databaseUrl);
  log(chalk`{green MongoDB connection successful.}`);

  const app = createApp(options);
  const server = http.createServer(app);
  await promisify(server.listen).bind(server)(options.port);

  const serverUrl = `http://localhost:${server.address().port}/admin`;
  log(chalk`{green Server started. Listening on: }{cyan ${serverUrl}}`);

  return {
    app,
    server,

    async stopServer() {
      await promisify(server.close).bind(server)();
      await mongoose.connection.close();
      log(chalk`{cyan Server closed}`);
    }
  };
}

if (process.argv[2] === 'start') {
  startServer();
}
