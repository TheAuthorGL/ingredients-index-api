import { ref, watch } from 'vue';

export function makeResourceIndexState(requests) {
  const loadingResources = ref(true);
  const resourcePaginationData = ref({});
  const resourcePage = ref(1);
  const resourceQuery = ref('');

  const fetchResources = async () => {
    loadingResources.value = true;

    try {
      resourcePaginationData.value = await requests.fetchResources({
        page: resourcePage.value,
        query: resourceQuery.value
      });
    } finally {
      loadingResources.value = false;
    }
  };

  const processingDeleteRequest = ref(false);

  const onResourceDeleted = async (ingredient) => {
    processingDeleteRequest.value = true;

    try {
      await requests.deleteResource(ingredient._id);
      await fetchResources();
    } finally {
      processingDeleteRequest.value = false;
    }
  };

  watch([ resourcePage, resourceQuery ], fetchResources);

  const navigateToNextPage = () => {
    resourcePage.value += 1;
  };

  const navigateToPreviousPage = () => {
    resourcePage.value -= 1;
  };

  fetchResources();

  return {
    loadingResources,
    resourcePaginationData,
    resourcePage,
    resourceQuery,
    fetchResources,

    processingDeleteRequest,
    onResourceDeleted,

    navigateToNextPage,
    navigateToPreviousPage
  };
}
