import { createWebHistory, createRouter } from 'vue-router';

import { hasAuthenticationToken } from '@shared/authentication';
import Login from './components/login.vue';
import Dashboard from './components/dashboard.vue';
import UniversalSearch from './components/universal-search/universal-search.vue';
import Ingredients from './components/ingredients/ingredients.vue';
import IngredientDetails from './components/ingredients/ingredient-details.vue';
import Products from './components/products/products.vue';
import ProductDetails from './components/products/product-details.vue';
import DatabaseConfiguration from './components/configuration/database-configuration.vue';
import UserConfiguration from './components/configuration/user-configuration.vue';

const routes = [
  {
    name: 'login',
    path: '/admin/login',
    component: Login
  },

  {
    name: 'dashboard',
    path: '/admin',
    component: Dashboard
  },

  {
    name: 'universalSearch',
    path: '/admin/universal-search',
    component: UniversalSearch
  },
  {
    name: 'ingredients',
    path: '/admin/ingredients',
    component: Ingredients
  },
  {
    name: 'ingredientDetails',
    path: '/admin/ingredients/:ingredientId',
    props: true,
    component: IngredientDetails
  },

  {
    name: 'products',
    path: '/admin/products',
    component: Products
  },
  {
    name: 'productDetails',
    path: '/admin/products/:productId',
    props: true,
    component: ProductDetails
  },

  {
    name: 'databaseConfiguration',
    path: '/admin/configuration/database',
    component: DatabaseConfiguration
  },
  {
    name: 'userConfiguration',
    path: '/admin/users',
    component: UserConfiguration
  }
];

export const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !hasAuthenticationToken()) {
    next({ name: 'login' });
  } else {
    next();
  }
});
