import { createApp } from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import LoadingOverlay from '@shared/components/loading-overlay.vue';
import App from './app.vue';
import { router } from './router';
import './styles/index.scss';
import './fa-icons';

createApp(App)
  .use(router)
  .component('fa-icon', FontAwesomeIcon)
  .component('loading-overlay', LoadingOverlay)
  .mount('#app');
