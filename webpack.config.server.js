const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  entry: path.resolve(__dirname, './server/server.js'),
  target: 'node',
  output: {
    path: path.resolve(__dirname, './dist/server'),
    filename: 'server.js',
    libraryTarget: 'commonjs2'
  },
  resolve: {
    alias: {
      '@server': path.resolve(__dirname, 'server'),
      '@shared': path.resolve(__dirname, 'shared'),
      '@modules': path.resolve(__dirname, 'modules'),
      '@frontend': path.resolve(__dirname, 'frontend'),
      '@frontendStyles': path.resolve(__dirname, 'frontend/styles'),
      '@frontendComponents': path.resolve(__dirname, 'frontend/components')
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'istanbul-instrumenter-loader',
        options: { esModules: true },
        enforce: 'post',
        include: /(server|modules)/
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.(scss|css)$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  externals: Object.keys(require('./package.json').dependencies)
};
