import { Router } from 'express';

import { auth } from '@modules/authentication/middleware';
import { IngredientModel } from './ingredient-model';

export function createRouter() {
  const router = Router();

  router.get('/ingredients/:id', async (req, res) => {
    const result = await IngredientModel.findById(req.params.id);

    res.json(result);
  });

  router.delete('/ingredients/:id', [ auth ], async (req, res) => {
    await IngredientModel.findByIdAndDelete(req.params.id);

    res.status(204).end();
  });

  router.get('/ingredients', async (req, res) => {
    const query = {
      label: req.query.query ? new RegExp(`${req.query.query}`, 'i') : /.*/,
      _id: { $nin: (req.query.exclude || '').split(';').filter(Boolean) }
    };

    let result;

    if (req.query.all === 'true') {
      result = await IngredientModel.find(query);
    } else {
      result = await IngredientModel.paginate(query, { page: req.query.page || 1 });
    }

    res.json(result);
  });

  router.post('/ingredients', [ auth ], async (req, res) => {
    const result = await IngredientModel.create(req.body);

    res.status(201).json(result);
  });

  router.patch('/ingredients/:id', [ auth ], async (req, res) => {
    await IngredientModel.findById(req.params.id).updateOne(req.body);

    res.status(204).end();
  });

  return router;
}
