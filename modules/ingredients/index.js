import { IngredientModel } from './ingredient-model';
import { createRouter } from './router';

export const IngredientsModule = {
  models: {
    IngredientModel
  },
  createRouter
};
