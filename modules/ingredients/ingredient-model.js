import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const ingredientSchema = new mongoose.Schema(
  {
    label: { type: String, required: true },
    description: { type: String, default: 'No description ready yet.' },
    published: { type: Boolean, default: false },
    thumbnail: { type: String, default: null }
  },
  {
    collation: { locale: 'en', strength: 1 }
  }
);

ingredientSchema.plugin(mongoosePaginate);

export const IngredientModel = mongoose.model('Ingredient', ingredientSchema);
