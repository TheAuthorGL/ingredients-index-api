import { AuthenticationModule } from './authentication';
import { IngredientsModule } from './ingredients';
import { ProductsModule } from './products';
import { UniversalSearchModule } from './universal-search';
import { DatabaseConfigurationModule } from './database-configuration';

export const modules = [
  DatabaseConfigurationModule,
  AuthenticationModule,
  IngredientsModule,
  ProductsModule,
  UniversalSearchModule
];
