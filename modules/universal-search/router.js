import { Router } from 'express';
import mongoose from 'mongoose';

// TODO: How to add unit tests?
export function createRouter() {
  const router = Router();

  router.get('/universal-search', async (req, res) => {
    const query = req.query.query || '';

    if (!query) {
      res.json([]);
      return;
    }

    const makeProjectionConfiguration = (resourceType) => ({
      $project: {
        label: 1,
        description: 1,
        thumbnail: 1,
        confidenceScore: { $meta: 'searchScore' },
        resourceType,
        published: 1,
        ...(resourceType === 'product' ? { ingredients: 1 } : {})
      }
    });

    const sharedSearchConfiguration = {
      query,
      fuzzy: {
        maxEdits: 2,
        maxExpansions: 100
      }
    };

    let productResults = await mongoose.model('Product').aggregate([
      {
        $search: {
          index: 'products',
          text: {
            path: [ 'label', 'description', 'ingredientSearchLabels' ],
            ...sharedSearchConfiguration
          }
        },
      },

      makeProjectionConfiguration('product'),
    ]);
    productResults = await mongoose.model('Product').populate(productResults, { path: 'ingredients' });

    const ingredientResults = await mongoose.model('Ingredient').aggregate([
      {
        $search: {
          index: 'ingredients',
          text: {
            path: [ 'label', 'description' ],
            ...sharedSearchConfiguration
          }
        },
      },

      makeProjectionConfiguration('ingredient'),
    ]);

    const result = [ ...productResults, ...ingredientResults ]
      .sort((a, b) => b.confidenceScore - a.confidenceScore)
      .filter((result) => result.published);

    res.json(result);
  });

  return router;
}
