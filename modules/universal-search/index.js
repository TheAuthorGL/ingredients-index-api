import { createRouter } from './router';

export const UniversalSearchModule = {
  createRouter
};
