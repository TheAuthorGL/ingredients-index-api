import mongoose from 'mongoose';

export async function setupDefaultUser() {
  const UserModel = mongoose.model('User');
  const userCount = await UserModel.estimatedDocumentCount();

  if (userCount !== 0) {
    return;
  }

  await UserModel.create({
    email: 'admin',
    password: 'admin'
  });
}
