import { createRouter } from './router';
import { setupPassportMiddleware } from './middleware';
import { setupDefaultUser } from './setup-default-user';

export const AuthenticationModule = {
  createRouter,

  setup(options) {
    setupPassportMiddleware(options.jwtSecret);
    setupDefaultUser();
  }
};
