import { Router } from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';

import { auth } from '@modules/authentication/middleware';
import { UserModel } from './user-model';

export function createRouter(options) {
  const router = Router();

  router.post('/auth/login', async (req, res, next) => {
    passport.authenticate('login', async (error, user) => {
      if (!user) {
        res.status(401).send('Invalid credentials');
        return;
      }

      await req.login(user, { session: false });

      const body = {
        user: { email: user.email }
      };

      res.json({
        token: jwt.sign(body, options.jwtSecret)
      });
    })(req, res, next);
  });

  router.get('/auth/user', [ auth ], async (req, res) => {
    res.json(req.user);
  });

  // TODO: Add get user by ID, patch user password, and delete user.

  router.get('/users', [ auth ], async (req, res) => {
    const result = await UserModel.paginate(
      {
        email: req.query.query ? new RegExp(`${req.query.query}`, 'i') : /.*/
      },
      { page: req.query.page || 1 }
    );

    res.json(result);
  });

  return router;
}
