import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { UserModel } from './user-model';

function setupPassportLoginMiddleware() {
  passport.use('login', new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password'
    },
    async (email, password, done) => {
      const user = await UserModel.findOne({ email });

      if (!user || !(await user.isPasswordValid(password))) {
        return done(null, null);
      }

      return done(null, user);
    }
  ));
}

function setupPassportJwtMiddleware(jwtSecret) {
  passport.use('jwt', new JwtStrategy(
    {
      secretOrKey: jwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    async (payload, done) => {
      const user = await UserModel
        .findOne({ email: payload.user.email })
        .select('email');

      done(null, user || false);
    }
  ));
}

export function setupPassportMiddleware(jwtSecret) {
  setupPassportLoginMiddleware();
  setupPassportJwtMiddleware(jwtSecret);
}

export const auth = passport.authenticate('jwt', { session: false });
