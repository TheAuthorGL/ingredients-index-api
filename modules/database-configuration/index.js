import { createRouter } from './router';

export const DatabaseConfigurationModule = {
  createRouter
};
