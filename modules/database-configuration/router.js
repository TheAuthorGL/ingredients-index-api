import { Router } from 'express';
import mongoose from 'mongoose';

import { auth } from '@modules/authentication/middleware';
import { seedDatabase } from '../../seed-database';

export function createRouter() {
  const router = Router();

  router.get('/database-configuration/reset', [ auth ], async (req, res) => {
    await mongoose.connection.dropDatabase();

    res.status(204).end();
  });

  router.get('/database-configuration/seed', [ auth ], async (req, res) => {
    await seedDatabase();

    res.status(204).end();
  });

  return router;
}
