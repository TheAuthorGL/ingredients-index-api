import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const productSchema = new mongoose.Schema(
  {
    label: { type: String, required: true },
    description: { type: String, default: 'No description ready yet.' },
    published: { type: Boolean, default: false },
    ingredients: [ { type: 'ObjectId', ref: 'Ingredient', required: true } ],
    ingredientSearchLabels: { type: String, default: '' },
    thumbnail: { type: String, default: null },
    externalLinkId: { type: String, default: null }
  },
  {
    collation: { locale: 'en', strength: 1 }
  }
);

productSchema.plugin(mongoosePaginate);

productSchema.set('toJSON', { virtuals: true });

productSchema.virtual('externalLink').get(function () {
  const linkTemplate = process.env.PRODUCT_LINK_TEMPLATE;

  if (!linkTemplate || !this.externalLinkId) {
    return null;
  }

  return linkTemplate.replace(/{([^{}]+)}/g, (keyExpr, key) => ({
    linkId: this.externalLinkId
  }[key] || ''));
});

productSchema.methods.updateIngredientSearchLabels = async function () {
  const ingredients = await mongoose.model('Ingredient').find({ _id: { $in: this.ingredients } });

  return mongoose.model('Product').findOneAndUpdate({ _id: this._id }, {
    ingredientSearchLabels: ingredients.map((ingredient) => ingredient.label).join(' ')
  });
};

export const ProductModel = mongoose.model('Product', productSchema);
