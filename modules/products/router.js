import { Router } from 'express';

import { auth } from '@modules/authentication/middleware';
import { ProductModel } from './product-model';

export function createRouter() {
  const router = Router();

  router.get('/products/:id', async (req, res) => {
    const result = await ProductModel.findById(req.params.id)
      .populate('ingredients');

    res.json(result);
  });

  router.delete('/products/:id', [ auth ], async (req, res) => {
    await ProductModel.findByIdAndDelete(req.params.id);

    res.status(204).end();
  });

  router.get('/products', async (req, res) => {
    const result = await ProductModel.paginate(
      {
        label: req.query.query ? new RegExp(`${req.query.query}`, 'i') : /.*/
      },
      { page: req.query.page || 1 }
    );

    res.json(result);
  });

  router.post('/products', [ auth ], async (req, res) => {
    const result = await ProductModel.create(req.body);

    res.status(201).json(result);
  });

  router.patch('/products/:id', [ auth ], async (req, res) => {
    const product = await ProductModel.findOneAndUpdate({ _id: req.params.id }, req.body, { new : true });
    await product.updateIngredientSearchLabels();

    res.status(204).end();
  });

  return router;
}
