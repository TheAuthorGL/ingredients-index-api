import { ProductModel } from './product-model';
import { createRouter } from './router';

export const ProductsModule = {
  models: {
    ProductModel
  },
  createRouter
};
