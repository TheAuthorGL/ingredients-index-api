# Ingredients Index API

## Local Environment Setup

- Create a `.env` file by creating a copy of `.env.example`

- Install Node dependencies: `npm install`.

- Build JS dist files and start the server: `npm run dev`. This should automatically open the app's admin panel with your favorite browser.

- Run unit tests: `npm run test`.

## MongoDB Atlas Cluster
The Universal Search functionality currently requires at least two MongoDB Atlas Search indexes to function properly, `products` and `ingredients`.

The search indexes must declare the following collections and fields, non-dynamic and string-typed:

**products**:
- `label`
- `description`
- `ingredientSearchLabels`

**ingredients**:
- `label`
- `description`
