const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { EnvironmentPlugin } = require('webpack');

require('dotenv').config();

module.exports = {
  entry: './admin/index.js',
  output: {
    path: path.resolve(__dirname, './dist/admin'),
    filename: 'admin.js'
  },
  resolve: {
    alias: {
      '@shared': path.resolve(__dirname, 'shared'),
      '@src': path.resolve(__dirname, 'admin'),
      '@styles': path.resolve(__dirname, 'admin/styles'),
      '@components': path.resolve(__dirname, 'admin/components')
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.(scss|css)$/,
        use: [
          'vue-style-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: false
            }
          },
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'styles.css'
    }),
    new EnvironmentPlugin([
      'API_URL'
    ])
  ]
};
