import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSearch,
  faBullseye,
  faLeaf,
  faPrescriptionBottle
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faSearch,
  faBullseye,
  faLeaf,
  faPrescriptionBottle
);
