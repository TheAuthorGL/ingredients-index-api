import { createSSRApp } from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import LoadingOverlay from '@shared/components/loading-overlay.vue';
import App from './app.vue';
import { router } from './router';
import { i18nPlugin } from './i18n';
import './fa-icons';

export function createFrontendVueApp() {
  const app = createSSRApp(App)
    .use(router)
    .use(i18nPlugin)
    .component('loading-overlay', LoadingOverlay)
    .component('fa-icon', FontAwesomeIcon);

  return {
    app,
    router
  };
}
