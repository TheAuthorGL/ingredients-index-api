import EnUs from './en-us.json';
import Es from './es.json';

export const messages = {
  'en-US': EnUs,
  'es': Es
};
