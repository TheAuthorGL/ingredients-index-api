import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';

import Homepage from './components/homepage/homepage.vue';
import UniversalSearchResults from './components/universal-search-results/universal-search-results.vue';
import IngredientDetails from './components/ingredients/ingredient-details.vue';
import ProductDetails from './components/products/product-details.vue';

const routes = [
  {
    name: 'homepage',
    path: '/',
    component: Homepage
  },
  {
    name: 'universalSearchResults',
    path: '/search',
    component: UniversalSearchResults
  },

  {
    name: 'ingredientDetails',
    path: '/ingredient/:ingredientId',
    props: true,
    component: IngredientDetails
  },
  {
    name: 'productDetails',
    path: '/product/:productId',
    props: true,
    component: ProductDetails
  }
];

// TODO: This should export a function instead.
export const router = createRouter({
  history: typeof window === 'undefined' ? createMemoryHistory() : createWebHistory(),
  routes
});
