import { createFrontendVueApp } from './create-app';
import './styles/index.scss';

(async () => {
  const { app, router } = createFrontendVueApp();
  await router.isReady();
  app.mount('#app');
})();
