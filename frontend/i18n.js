import { createI18n } from 'vue-i18n';
import { messages } from './i18n-messages';

export const i18nPlugin = createI18n({
  legacy: false,
  globalInjection: true,
  locale: process.env.DEFAULT_LOCALE,
  messages
});
