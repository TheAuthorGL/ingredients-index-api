const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { EnvironmentPlugin } = require('webpack');

require('dotenv').config();

module.exports = {
  entry: './frontend/index.js',
  output: {
    path: path.resolve(__dirname, './dist/frontend'),
    filename: 'frontend.js'
  },
  resolve: {
    alias: {
      '@shared': path.resolve(__dirname, 'shared'),
      '@frontend': path.resolve(__dirname, 'frontend'),
      '@frontendStyles': path.resolve(__dirname, 'frontend/styles'),
      '@frontendComponents': path.resolve(__dirname, 'frontend/components')
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.(scss|css)$/,
        use: [
          'vue-style-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: false
            }
          },
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'styles.css'
    }),
    new EnvironmentPlugin({
      'API_URL': undefined,
      'DEFAULT_LOCALE': 'en-US'
    })
  ]
};
