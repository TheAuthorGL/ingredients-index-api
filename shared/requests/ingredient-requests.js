import { makeGet, makePost, makePatch, makeDelete } from '@shared/requests';

export const fetchIngredient = makeGet((ingredientId) => `/ingredients/${ingredientId}`);
export const fetchIngredients = makeGet('/ingredients');
export const createIngredient = makePost('/ingredients');
export const patchIngredient = makePatch((ingredientId) => `/ingredients/${ingredientId}`);
export const deleteIngredient = makeDelete((ingredientId) => `/ingredients/${ingredientId}`);
