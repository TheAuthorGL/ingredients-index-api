import { makeGet, makePost } from '@shared/requests';

export const fetchAuthenticatedUser = makeGet('/auth/user');
export const loginWithCredentials = makePost('/auth/login');

export const fetchUsers = makeGet('/users');
