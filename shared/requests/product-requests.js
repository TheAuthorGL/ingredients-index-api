import { makeGet, makePost, makePatch, makeDelete } from '@shared/requests';

export const fetchProduct = makeGet((productId) => `/products/${productId}`);
export const fetchProducts = makeGet('/products');
export const createProduct = makePost('/products');
export const patchProduct = makePatch((productId) => `/products/${productId}`);
export const deleteProduct = makeDelete((productId) => `/products/${productId}`);
