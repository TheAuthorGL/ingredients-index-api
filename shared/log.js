export function log(message) {
  if (process.env.SILENT !== 'true') {
    console.log(message);
  }
}
