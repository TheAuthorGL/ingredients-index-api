import { ref } from 'vue';

const tokenStorageKey = 'authenticationJwt';

export const authenticationToken = ref(hasAuthenticationToken() && getAuthenticationToken());

export function saveAuthenticationToken(token) {
  localStorage.setItem(tokenStorageKey, token);
  authenticationToken.value = token;
}

export function removeAuthenticationToken() {
  localStorage.removeItem(tokenStorageKey);
  authenticationToken.value = undefined;
}

export function getAuthenticationToken() {
  return localStorage.getItem(tokenStorageKey);
}

export function hasAuthenticationToken() {
  return typeof window !== 'undefined' && Boolean(localStorage.getItem(tokenStorageKey));
}
