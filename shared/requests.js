import { getAuthenticationToken, hasAuthenticationToken } from './authentication';

const doFetch = typeof window === 'undefined'
  ? require('node-fetch')
  : window.fetch;

const jsonHeaders = {
  'Accept': 'application/json, text/plain, */*',
  'Content-Type': 'application/json'
};

function getAuthorizationHeader() {
  if (!hasAuthenticationToken()) {
    return {};
  }

  return {
    'Authorization': `Bearer ${getAuthenticationToken()}`
  };
}

async function handleRequestResponse(response) {
  const responseContentType = response.headers.get('content-type');
  const responseHasJson = responseContentType && responseContentType.indexOf('application/json') !== -1;
  const responseContent = await (responseHasJson ? response.json() : response.text());

  if (response.ok) {
    return responseContent;
  }

  // TODO: If the response code starts with 5, different handling is needed.
  const error = new Error();
  error.response = response;
  throw error;
}

function resolveUrl(url, parameters) {
  const urlSuffix = (typeof url === 'function'
    ? url(...parameters)
    : `${url}?${new URLSearchParams(parameters[0])}`);

  return `${process.env.API_URL}${urlSuffix}`;
}

function makeRequest(method) {
  return (url) => async (...parameters) => {
    const response = await doFetch(resolveUrl(url, parameters), {
      method,
      headers: { ...getAuthorizationHeader() }
    });

    return handleRequestResponse(response);
  };
}

function makeRequestWithBody(method) {
  return (url) => async (...parametersAndBody) => {
    const body = parametersAndBody[parametersAndBody.length - 1];
    const parameters = parametersAndBody.slice(0, -1);

    const response = await doFetch(resolveUrl(url, parameters), {
      method,
      headers: { ...jsonHeaders, ...getAuthorizationHeader() },
      body: JSON.stringify(body)
    });

    return handleRequestResponse(response);
  };
}

export const makeGet = makeRequest('GET');
export const makeDelete = makeRequest('DELETE');
export const makePost = makeRequestWithBody('POST');
export const makePatch = makeRequestWithBody('PATCH');
