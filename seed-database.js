import mongoose from 'mongoose';
import chalk from 'chalk';
import faker from 'faker';

import { log } from './shared/log';

// TODO: Make this runnable with NPM, and move it somewhere else.
export async function seedDatabase() {
  log(chalk`{gray Seeding database...}`);

  await mongoose.model('User').create({
    email: 'test@user.com',
    password: 'test'
  });

  // TODO: find a way to generalize seeders of this kind
  await mongoose.model('Ingredient').create({
    label: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    published: true
  });

  await mongoose.model('Product').create({
    label: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    published: true
  });

  log(chalk`{green Database successfully seeded.}`);
}
