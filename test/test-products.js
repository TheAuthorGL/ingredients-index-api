import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { refreshDatabase, startTestServer } from './shared/server-helpers';
import { makeModelHelpers } from './shared/model-helpers';
import { getAuthorizationHeader } from './shared/authentication-helpers';

chai.use(chaiHttp);

describe('Products API', function () {
  let server;

  before(async () => { server = await startTestServer(); });
  beforeEach(refreshDatabase);
  after(async () => await server.stopServer());

  const [ ProductModel, sampleProduct, getProduct ] = makeModelHelpers('Product');

  describe('GET /api/products/:id', function () {
    it('should serve a specific product by id', async function () {
      const product = await sampleProduct();
      const res = await chai.request(server.app).get(`/api/products/${product._id}`);

      expect(res).to.be.json;
      expect(res).to.have.status(200);
      expect(String(res.body._id)).to.equal(String(product._id));
    });
  });

  describe('DELETE /api/product/:id', function () {
    it('should delete a specific product by id', async function () {
      const product = await sampleProduct();
      const res = await chai.request(server.app).delete(`/api/products/${product._id}`)
        .set(getAuthorizationHeader());
      const productExists = await ProductModel.exists({ _id: product._id });

      expect(res).to.have.status(204);
      expect(res.body).to.be.empty;
      expect(productExists).to.equal(false);
    });
  });

  describe('GET /api/products', async function () {
    it('should serve products', async function () {
      const res = await chai.request(server.app).get('/api/products?');

      expect(res).to.be.json;
      expect(res).to.have.status(200);
      expect(res.body.docs).to.not.be.empty;
    });
  });

  describe('POST /api/products', async function () {
    it('should create a new product', async function () {
      const res = await chai.request(server.app).post('/api/products')
        .send({ label: 'test' })
        .set(getAuthorizationHeader());
      const newProduct = await getProduct(res.body._id);

      expect(res).to.be.json;
      expect(res).to.have.status(201);
      expect(res.body).to.include.keys([ '_id' ]);
      expect(newProduct).to.not.be.null;
      expect(newProduct.label).to.equal('test');
    });
  });

  describe('PATCH /api/products/:id', function () {
    it('should update a product by id', async function () {
      const product = await sampleProduct();
      const fieldsToUpdate = {
        label: `${product.label}abc`
      };
      const res = await chai.request(server.app).patch(`/api/products/${product._id}`)
        .send(fieldsToUpdate)
        .set(getAuthorizationHeader());
      const updatedProduct = await getProduct(product._id);

      expect(res).to.have.status(204);
      expect(res.body).to.be.empty;
      expect(updatedProduct.label).to.equal(fieldsToUpdate.label);
    });
  });
});
