import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { refreshDatabase, startTestServer } from './shared/server-helpers';

chai.use(chaiHttp);

describe('Server', function () {
  let server;

  before(async () => { server = await startTestServer(); });
  beforeEach(refreshDatabase);
  after(async () => await server.stopServer());

  it('should start and serve any content on GET /', async function () {
    const res = await chai.request(server.app).get('/');
    expect(res).to.have.status(200);
  });

  // TODO: Some of these could be part of separate modules

  it('should serve robots.txt', async function () {
    const res = await chai.request(server.app).get('/robots.txt');
    expect(res).to.have.status(200);
    expect(res).to.be.text;
  });

  it('should serve the admin panel on /', async function() {
    const res = await chai.request(server.app).get('/admin');
    expect(res).to.have.status(200);
  });
});
