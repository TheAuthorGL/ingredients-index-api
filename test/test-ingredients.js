import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { refreshDatabase, startTestServer } from './shared/server-helpers';
import { makeModelHelpers } from './shared/model-helpers';
import { getAuthorizationHeader } from './shared/authentication-helpers';

chai.use(chaiHttp);

describe('Ingredients API', function () {
  let server;

  before(async () => { server = await startTestServer(); });
  beforeEach(refreshDatabase);
  after(async () => await server.stopServer());

  const [ IngredientModel, sampleIngredient, getIngredient ] = makeModelHelpers('Ingredient');

  describe('GET /api/ingredients/:id', function () {
    it('should serve a specific ingredient by id', async function () {
      const ingredient = await sampleIngredient();
      const res = await chai.request(server.app).get(`/api/ingredients/${ingredient._id}`);

      expect(res).to.be.json;
      expect(res).to.have.status(200);
      expect(String(res.body._id)).to.equal(String(ingredient._id));
    });
  });

  describe('DELETE /api/ingredients/:id', function () {
    it('should delete a specific ingredient by id', async function () {
      const ingredient = await sampleIngredient();
      const res = await chai.request(server.app).delete(`/api/ingredients/${ingredient._id}`)
        .set(getAuthorizationHeader());
      const ingredientExists = await IngredientModel.exists({ _id: ingredient._id });

      expect(res).to.have.status(204);
      expect(res.body).to.be.empty;
      expect(ingredientExists).to.equal(false);
    });
  });

  describe('GET /api/ingredients', function () {
    it('should serve ingredients', async function () {
      const res = await chai.request(server.app).get('/api/ingredients?');

      expect(res).to.be.json;
      expect(res).to.have.status(200);
      expect(res.body.docs).to.not.be.empty;
    });
  });

  describe('POST /api/ingredients', function () {
    it('should create a new ingredient', async function () {
      const res = await chai.request(server.app).post('/api/ingredients')
        .send({ label: 'test' })
        .set(getAuthorizationHeader());
      const newIngredient = await getIngredient(res.body._id);

      expect(res).to.be.json;
      expect(res).to.have.status(201);
      expect(res.body).to.include.keys([ '_id' ]);
      expect(newIngredient).to.not.be.null;
      expect(newIngredient.label).to.equal('test');
    });
  })

  describe('PATCH /api/ingredients/:id', function () {
    it('should update an ingredient', async function () {
      const ingredient = await sampleIngredient();
      const fieldsToUpdate = {
        label: `${ingredient.label}abc`
      };
      const res = await chai.request(server.app).patch(`/api/ingredients/${ingredient._id}`)
        .send(fieldsToUpdate)
        .set(getAuthorizationHeader());
      const updatedIngredient = await getIngredient(ingredient._id);

      expect(res).to.have.status(204);
      expect(res.body).to.be.empty;
      expect(updatedIngredient.label).to.equal(fieldsToUpdate.label);
    });
  });
});
