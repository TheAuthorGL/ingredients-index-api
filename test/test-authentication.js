import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { refreshDatabase, startTestServer } from './shared/server-helpers';
import { getAuthorizationHeader } from './shared/authentication-helpers';

chai.use(chaiHttp);

describe('API Authentication', function () {
  let server;

  before(async () => { server = await startTestServer(); });
  beforeEach(refreshDatabase);
  after(async () => await server.stopServer());

  it('should protect routes when no valid authentication is provided', async function () {
    const res = await chai.request(server.app).get('/api/auth/user');
    expect(res).to.have.status(401);
  });

  describe('GET /api/auth/user', function () {
    it('should serve authenticated user information', async function () {
      const res = await chai.request(server.app).get('/api/auth/user')
        .set(getAuthorizationHeader());

      expect(res).to.have.status(200);
      expect(res).to.be.json;
      expect(res.body.email).to.equal('test@user.com');
    });
  });

  describe('POST /login', function () {
    it('should provide an authentication token for valid credentials', async function () {
      const res = await chai.request(server.app).post('/api/auth/login')
        .send({ email: 'test@user.com', password: 'test' });

      expect(res).to.be.json;
      expect(res).to.have.status(200);
      expect(res.body).to.include.keys([ 'token' ]);
    });

    it('should reject invalid credentials', async function () {
      const res = await chai.request(server.app).post('/api/auth/login')
        .send({ email: 'test@user.com', password: 'incorrect' });

      expect(res).to.have.status(401);
    });
  });

});
