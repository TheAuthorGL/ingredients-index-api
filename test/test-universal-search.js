import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { refreshDatabase, startTestServer } from './shared/server-helpers';

chai.use(chaiHttp);

describe('Universal Search API', function () {
  let server;

  before(async () => {
    server = await startTestServer();
  });
  beforeEach(refreshDatabase);
  after(async () => await server.stopServer());

  describe('GET /api/universal-search', function () {
    it('should serve nothing if no query provided', async function () {
      const res = await chai.request(server.app).get('/api/universal-search');
      expect(res).to.be.json;
      expect(res.body).to.be.empty;
    });
  });
});
