import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { refreshDatabase, startTestServer } from './shared/server-helpers';
import { getAuthorizationHeader } from './shared/authentication-helpers';

chai.use(chaiHttp);

describe('Database Configuration API', function () {
  let server;

  before(async () => {
    server = await startTestServer();
  });
  beforeEach(refreshDatabase);
  after(async () => await server.stopServer());

  describe('GET /api/database-configuration/reset', function () {
    it('should reset the database', async function () {
      const res = await chai.request(server.app).get('/api/database-configuration/reset')
        .set(getAuthorizationHeader());

      expect(res).to.have.status(204);
      expect(res.body).to.be.empty;
    });
  });

  describe('GET /api/database-configuration/reset', function () {
    it('should seed the database', async function () {
      const res = await chai.request(server.app).get('/api/database-configuration/seed')
        .set(getAuthorizationHeader());

      expect(res).to.have.status(204);
      expect(res.body).to.be.empty;
    });
  });
});
