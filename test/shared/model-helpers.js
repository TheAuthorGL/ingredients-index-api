import mongoose from 'mongoose/index';

export function makeModelHelpers(modelName) {
  const model = mongoose.model(modelName);

  return [
    model,
    async () => (await model.findOne({})).toObject(),
    async (id) => (await model.findById(id)).toObject()
  ];
}
