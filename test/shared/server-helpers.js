import mongoose from 'mongoose';

import { startServer } from '../../dist/server/server.js';
import { seedDatabase } from '../../seed-database';

export async function refreshDatabase() {
  await mongoose.connection.dropDatabase();
  await seedDatabase();
}

export async function startTestServer() {
  process.env.SILENT = true;

  return startServer({
    databaseUrl: process.env.TEST_DATABASE_URL,
    openAdminPage: false,
    jwtSecret: 'secret'
  });
}
