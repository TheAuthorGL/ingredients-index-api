import jwt from 'jsonwebtoken';

export function getAuthorizationHeader() {
  const body = {
    user: { email: 'test@user.com' }
  };

  const token = jwt.sign(body, 'secret');

  return { 'Authorization': `Bearer ${token}` };
}
